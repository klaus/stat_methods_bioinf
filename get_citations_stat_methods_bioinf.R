library(knitcitations)

library(bibtex)



# Kyewski and Klein, 2006
citep("10.1146/annurev.immunol.23.021704.115601")

# Brennecke et. al., 2015
citep("10.1038/ni.3246")

# Pinto et. al. 2013
citep("10.1073/pnas.1308311110")

# recount2 resource
citep("10.1038/nbt.3838")

# Löhr et. al. 2013
citep("10.1371/journal.pone.0067461")

# Brennecke et. al., 2013
citep("10.1038/nmeth.2645")

# [Lun et. al.](http://dx.doi.org/10.1186/s13059-016-0947-7)
citep("10.1186/s13059-016-0947-7")

# cf. Leek et. al. 2010
citep("10.1038/nrg2825")

# [Buettner et. al.](http://dx.doi.org/10.1038/nbt.3102)
citep("10.1038/nbt.3102")

# Buja et. al. , 2007
citep("10.1198/106186008X318440")

# Kruskal, 1964
citep("10.1007/BF02289565")


#citep("10.1101/125112")

# Perraudeau et. al. , 2017
citep("10.12688/f1000research.12122.1")


# Nygaard et. al, 2015
citep("10.1093/biostatistics/kxv027")

# Jaffe et. al., 2015
citep("10.1186/s12859-015-0808-5")

# Julia et. al. 2015
#citep("10.1093/bioinformatics/btv368")

# Novembre and Stephens, 2008
citep("10.1038/ng.139")

# Shaffer, 1995

citep("10.1146/annurev.ps.46.020195.003021")

# Schäfer and Strimmer, 2005

citep("10.2202/1544-6115.1175")


# Ahdesmäki and Strimmer, 2010

citep("10.1214/09-AOAS277")


# Finak et. al., 2015

citep("10.1186/s13059-015-0844-5")

# Risso et. al., 2018
 citep("10.1038/s41467-017-02554-5")


# add_manually("@article {Risso_2017,
#   author = {Risso, Davide and Perraudeau, Fanny and Gribkova, Svetlana and Dudoit, Sandrine and Vert, Jean-Philippe},
#   title = {ZINB-WaVE: A general and flexible method for signal extraction from single-cell RNA-seq data},
#   year = {2017},
#   doi = {10.1101/125112},
#   publisher = {Cold Spring Harbor Laboratory},
#   abstract = {Single-cell RNA sequencing (scRNA-seq) is a powerful high-throughput technique that enables researchers to measure genome-wide transcription levels at the resolution of single cells. Because of the low amount of RNA present in a single cell, some genes may fail to be detected even though they are expressed; these genes are usually referred to as dropouts. Here, we present a general and flexible zero-inflated negative binomial model (ZINB-WaVE), which leads to low-dimensional representations of the data that account for zero inflation (dropouts), over-dispersion, and the count nature of the data. We demonstrate, with simulated and real data, that the model and its associated estimation procedure are able to give a more stable and accurate low-dimensional representation of the data than principal component analysis (PCA) and zero-inflated factor analysis (ZIFA), without the need for a preliminary normalization step.},
#   URL = {https://www.biorxiv.org/content/early/2017/11/02/125112},
#   eprint = {https://www.biorxiv.org/content/early/2017/11/02/125112.full.pdf},
#   journal = {bioRxiv}
# }")

# Buettner et. al. 2017
 
citep("10.1186/s13059-017-1334-8")
 
# Angermüller et. al., 2016

citep("10.15252/msb.20156651")



### export citations

write.bibtex(file = "stat_methods_bioinf.bib")
knitcitations::cleanbib()



add_manually <- function(entry){
  
  write("\n", file = "stat_methods_bioinf.bib", append = TRUE)
  write(entry, file = "stat_methods_bioinf.bib", append = TRUE)
  write("\n", file = "stat_methods_bioinf.bib", append = TRUE)
  
}

# Goodfellow et. al. , 2016

add_manually("@book{Goodfellow_2016,
  title={Deep Learning},
  author={Ian Goodfellow and Yoshua Bengio and Aaron Courville},
  publisher={MIT Press},
  note={\\url{http://www.deeplearningbook.org}},
  year={2016}
}")

#  van der Maaten and Hinton, 2008

add_manually("@article{vanDerMaaten_2008,
  author = {van der Maaten, Laurens and Hinton, Geoffrey},
  interhash = {370ba8b9e1909b61880a6f47c93bcd49},
  intrahash = {8b9aebb404ad4a4c6a436ea413550b30},
  journal = {Journal of Machine Learning Research},
  pages = {2579--2605},
  title = {Visualizing Data using {t-SNE} },
  url = {http://www.jmlr.org/papers/v9/vandermaaten08a.html},
  volume = 9,
  year = 2008
}")



# Benjamini and Hochberg, 1995
add_manually("@article{Benjamini_1995,
  ISSN = {00359246},
  URL = {http://www.jstor.org/stable/2346101},
  abstract = {The common approach to the multiplicity problem calls for controlling the familywise error rate (FWER). This approach, though, has faults, and we point out a few. A different approach to problems of multiple significance testing is presented. It calls for controlling the expected proportion of falsely rejected hypotheses-the false discovery rate. This error rate is equivalent to the FWER when all hypotheses are true but is smaller otherwise. Therefore, in problems where the control of the false discovery rate rather than that of the FWER is desired, there is potential for a gain in power. A simple sequential Bonferroni-type procedure is proved to control the false discovery rate for independent test statistics, and a simulation study shows that the gain in power is substantial. The use of the new procedure and the appropriateness of the criterion are illustrated with examples.},
  author = {Yoav Benjamini and Yosef Hochberg},
  journal = {Journal of the Royal Statistical Society. Series B (Methodological)},
  number = {1},
  pages = {289-300},
  publisher = {[Royal Statistical Society, Wiley]},
  title = {Controlling the False Discovery Rate: A Practical and Powerful Approach to Multiple Testing},
  volume = {57},
  year = {1995}
}")

# Street et. al., 2017

add_manually("@article {Street_2017,
  author = {Street, Kelly and Risso, Davide and Fletcher, Russell B and Das, Diya and Ngai, John and Yosef, Nir and Purdom, Elizabeth and Dudoit, Sandrine},
  title = {Slingshot: Cell lineage and pseudotime inference for single-cell transcriptomics},
  year = {2017},
  doi = {10.1101/128843},
  publisher = {Cold Spring Harbor Laboratory},
  abstract = {Single-cell transcriptomics allows researchers to investigate complex communities of heterogeneous cells. These methods can be applied to stem cells and their descendants in order to chart the progression from multipotent progenitors to fully differentiated cells. While a number of statistical and computational methods have been proposed for analyzing cell lineages, the problem of accurately characterizing multiple branching lineages remains difficult to solve. Here, we introduce a novel method, Slingshot, for inferring multiple developmental lineages from single-cell gene expression data. Slingshot is a uniquely robust and flexible tool for inferring developmental lineages and ordering cells to reflect continuous, branching processes.},
  URL = {https://www.biorxiv.org/content/early/2017/04/19/128843},
  eprint = {https://www.biorxiv.org/content/early/2017/04/19/128843.full.pdf},
  journal = {bioRxiv}
}")

# Delgado et. al. . 2014

add_manually("@article {Delgado_14,
  author  = {Manuel Fern\'{a}ndez-Delgado and Eva Cernadas and Sen\'{e}n Barro and Dinani Amorim},
  title   = {Do we Need Hundreds of Classifiers to Solve Real World Classification Problems?},
  journal = {Journal of Machine Learning Research},
  year    = {2014},
  volume  = {15},
  pages   = {3133-3181},
  url     = {http://jmlr.org/papers/v15/delgado14a.html}
}")

# Argelaguet et. al. 2017

add_manually("@article {Argelaguet_2017,
  author = {Argelaguet, Ricard and Velten, Britta and Arnol, Damien and Dietrich, Sascha and Zenz, Thorsten and Marioni, John C. and Buettner, Florian and Huber, Wolfgang and Stegle, Oliver},
  title = {Multi-Omics factor analysis disentangles heterogeneity in blood cancer},
  year = {2017},
  doi = {10.1101/217554},
  publisher = {Cold Spring Harbor Laboratory},
  abstract = {Multi-omic studies in large cohorts promise to characterize biological processes across molecular layers including genome, transcriptome, epigenome, proteome and perturbation phenotypes. However, methods for integrating multi-omic datasets are lacking. We present Multi-Omics Factor Analysis (MOFA), an unsupervised dimensionality reduction method for discovering the driving sources of variation in multi-omics data. Our model infers a set of (hidden) factors that capture biological and technical sources of variability across data modalities. We applied MOFA to data from 200 patient samples of chronic lymphocytic leukemia (CLL) profiled for somatic mutations, RNA expression, DNA methylation and ex-vivo responses to a panel of drugs. MOFA automatically discovered the known dimensions of disease heterogeneity, including immunoglobulin heavy chain variable region (IGHV) status and trisomy of chromosome 12, as well as previously underappreciated drivers of variation, such as response to oxidative stress. These factors capture key dimensions of patient heterogeneity, including those linked to clinical outcomes. Finally, MOFA handles missing data modalities in subsets of samples, enabling imputation, and the model can identify outlier samples.},
  URL = {https://www.biorxiv.org/content/early/2017/11/10/217554},
  eprint = {https://www.biorxiv.org/content/early/2017/11/10/217554.full.pdf},
  journal = {bioRxiv}
}")

# Jula et. al., 2015

add_manually("
@Article{Juli__2015,
  doi = {10.1093/bioinformatics/btv368},
  url = {https://doi.org/10.1093/bioinformatics/btv368},
  year = {2015},
  month = {jun},
  publisher = {Oxford University Press ({OUP})},
  volume = {31},
  number = {20},
  pages = {3380--3382},
  author = {Miguel Julia and Amalio Telenti and Antonio Rausell},
  title = {Sincell: an R/Bioconductor package for statistical assessment of cell-state hierarchies from single-cell {RNA}-seq: Fig. 1.},
  journal = {Bioinformatics},
}")


# Ching et. al. , 2018

add_manually("
@article {Ching_2018,
  author = {Ching, Travers and Himmelstein, Daniel S. and Beaulieu-Jones, Brett K. and Kalinin, Alexandr A. and Do, Brian T. and Way, Gregory P. and Ferrero, Enrico and Agapow, Paul-Michael and Zietz, Michael and Hoffman, Michael M and Xie, Wei and Rosen, Gail L. and Lengerich, Benjamin J. and Israeli, Johnny and Lanchantin, Jack and Woloszynek, Stephen and Carpenter, Anne E. and Shrikumar, Avanti and Xu, Jinbo and Cofer, Evan M. and Lavender, Christopher A and Turaga, Srinivas C and Alexandari, Amr M and Lu, Zhiyong and Harris, David J. and DeCaprio, Dave and Qi, Yanjun and Kundaje, Anshul and Peng, Yifan and Wiley, Laura K. and Segler, Marwin H. S. and Boca, Simina M and Swamidass, S. Joshua and Huang, Austin and Gitter, Anthony and Greene, Casey S.},
  title = {Opportunities And Obstacles For Deep Learning In Biology And Medicine},
  year = {2018},
  doi = {10.1101/142760},
  publisher = {Cold Spring Harbor Laboratory},
  abstract = {Deep learning, which describes a class of machine learning algorithms, has recently showed impressive results across a variety of domains. Biology and medicine are data rich, but the data are complex and often ill-understood. Problems of this nature may be particularly well-suited to deep learning techniques. We examine applications of deep learning to a variety of biomedical problems - patient classification, fundamental biological processes, and treatment of patients - and discuss whether deep learning will transform these tasks or if the biomedical sphere poses unique challenges. We find that deep learning has yet to revolutionize or definitively resolve any of these problems, but promising advances have been made on the prior state of the art. Even when improvement over a previous baseline has been modest, we have seen signs that deep learning methods may speed or aid human investigation. More work is needed to address concerns related to interpretability and how to best model each problem. Furthermore, the limited amount of labeled data for training presents problems in some domains, as do legal and privacy constraints on work with sensitive health records. Nonetheless, we foresee deep learning powering changes at both bench and bedside with the potential to transform several areas of biology and medicine.},
  URL = {https://www.biorxiv.org/content/early/2018/01/19/142760},
  eprint = {https://www.biorxiv.org/content/early/2018/01/19/142760.full.pdf},
  journal = {bioRxiv}
}")


add_manually("
@article {Chawla_2005,
  author = {Chawla, Nitesh V.},
  biburl = {https://www.bibsonomy.org/bibtex/282896d5075d60317023dde6a91287ace/dblp},
  booktitle = {The Data Mining and Knowledge Discovery Handbook},
  crossref = {books/sp/DM2005},
  date = {2005-09-05},
  description = {dblp},
  editor = {Maimon, Oded and Rokach, Lior},
  interhash = {efcb5e9d1ce4afc3525d08b6dd3ac2d4},
  intrahash = {82896d5075d60317023dde6a91287ace},
  isbn = {0-387-24435-2},
  keywords = {dblp},
  pages = {853-867},
  publisher = {Springer},
  timestamp = {2005-09-05T00:00:00.000+0200},
  title = {Data Mining for Imbalanced Datasets: An Overview.},
  year = 2005
}")


# Cole et. al. , 2018

add_manually("
@article {Cole_2018,
 author = {Cole, Michael B and Risso, Davide and Wagner, Allon and DeTomaso, David and Ngai, John and Purdom, Elizabeth and Dudoit, Sandrine and Yosef, Nir},
 title = {Performance Assessment and Selection of Normalization Procedures for Single-Cell RNA-Seq},
 year = {2017},
 doi = {10.1101/235382},
 publisher = {Cold Spring Harbor Laboratory},
 abstract = {Due to the presence of systematic measurement biases, data normalization is an essential preprocessing step in the analysis of single-cell RNA sequencing (scRNA-seq) data. While a variety of normalization procedures are available for bulk RNA-seq, their suitability with respect to single-cell data is still largely unexplored. Furthermore, there may be multiple, competing considerations behind the assessment of normalization performance, some of them study-specific. The choice of normalization method can have a large impact on the results of downstream analyses (e.g., clustering, inference of cell lineages, differential expression analysis), and thus it is critically important to assess the performance of competing methods in order to select a suitable procedure for the study at hand. We have developed scone - a framework that implements a wide range of normalization procedures in the context of scRNA-seq, and enables the assessment of their performance based on a comprehensive set of data-driven performance metrics. The accompanying open-source Bioconductor R software package scone (available at https://bioconductor.org/packages/scone) also provides numerical and graphical summaries of expression measures, data quality assessment, and data-adaptive gene and sample filtering criteria. We demonstrate the effectiveness of scone on a selection of scRNA-seq datasets across a variety of protocols, ranging from plate- to droplet-based methods. We show that scone is able to correctly rank normalization methods according to their performance in a given dataset and that selecting the best performing normalization leads to higher agreement with independent validation data than lowly-ranked methods.},
 URL = {https://www.biorxiv.org/content/early/2017/12/16/235382},
 eprint = {https://www.biorxiv.org/content/early/2017/12/16/235382.full.pdf},
 journal = {bioRxiv}
}")
